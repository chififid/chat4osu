import { State, Action, StateContext, Selector, Store } from '@ngxs/store';
import produce from 'immer';
import { ChangeChannelName } from '../actions/channel.actions';
import { SendMessageToChannel } from '../actions/message.actions';
import { Logout } from '../actions/auth.actions';
import {
  AddUser,
  RemoveUser,
  JoinMpLobby,
  LeaveMpLobby,
  MoveUser,
  SetTeam,
  SetRoomName,
  SetTeamMode,
  SetWinCondition,
  SetFreeMod,
  SetMods,
  SetBeatmap,
  SetTeamScore,
  RemoveTeamScore,
  SetAliasTeamScore,
  ReInitTeamScores,
  SetWrapCondition,
  SetAutoshow,
  SetAutowrap,
} from '../actions/multiplayer.actions';
import { ChannelStateModel, ChannelState } from './channel.state';
import { StorageService } from '../../providers/storage.service';
import { Injectable } from '@angular/core';
import { AddToast } from '../actions/toast.actions';

export enum WrapCondition { 
  Miss = 'Miss', 
  Accuracy = 'Accuracy',
  MissAccuracy = 'Miss&Accuracy',
  Score = 'Score',
  Combo = 'Combo',
};

export interface TeamObj {
  team: string;
}

export interface TeamScore {
  team: string;
  score: number;
  alias: string;
}

export interface MpUser {
  username: string;
  slot: number;
  team: string;
  host: boolean;
}

export interface MpLobby {
  mpId: string;
  roomName: string;
  teamMode: string;
  winCondition: string;
  wrapCondition: WrapCondition;
  mods: string[];
  freemod: boolean;
  players: MpUser[];
  beatmapId: string;
  status: string;
  scores: TeamScore[];
  autoshow: boolean;
  autowrap: boolean;
}

export interface MultiplayerStateModel {
  multiplayerLobbies: { [mpId: string]: MpLobby };
}

@State<MultiplayerStateModel>({
  name: 'multiplayer',
  defaults: {
    multiplayerLobbies: {}
  }
})
@Injectable()
export class MultiplayerState {
  @Selector([ChannelState])
  static lobby(state: MultiplayerStateModel, channelState: ChannelStateModel) {
    return state.multiplayerLobbies[channelState.currentChannel];
  }

  constructor(private store: Store, private storage: StorageService) { }

  @Action(JoinMpLobby)
  async joinLobby(
    ctx: StateContext<MultiplayerStateModel>,
    action: JoinMpLobby
  ) {
    // Save channel for client reopening
    const channels = this.storage.get('channels') || ['#osu'];
    if (channels.indexOf(action.payload) === -1) {
      this.storage.set('channels', [...channels, action.payload]);
    }

    const newLobby: MpLobby = {
      mpId: action.payload,
      roomName: '',
      teamMode: '',
      winCondition: '',
      wrapCondition: WrapCondition.Accuracy,
      mods: [],
      freemod: false,
      players: [],
      beatmapId: '',
      status: '',
      scores: [],
      autoshow: false,
      autowrap: false,
    };

    ctx.setState(
      produce(ctx.getState(), draft => {
        draft.multiplayerLobbies[action.payload] = newLobby;
      })
    );

    // Initialize channel settings
    ctx.dispatch(
      new SendMessageToChannel({
        channel: action.payload,
        message: '!mp settings',
        date: new Date()
      })
    );
  }

  @Action(LeaveMpLobby)
  async leaveLobby(
    ctx: StateContext<MultiplayerStateModel>,
    action: LeaveMpLobby
  ) {
    ctx.setState(
      produce(ctx.getState(), draft => {
        delete draft.multiplayerLobbies[action.payload];
      })
    );
  }

  @Action(AddUser)
  async addUser(ctx: StateContext<MultiplayerStateModel>, action: AddUser) {
    ctx.setState(
      produce(ctx.getState(), draft => {
        const lobby = draft.multiplayerLobbies[action.payload.channelName];

        const user: MpUser = lobby.players.find(
          e => e.username.toLowerCase() === action.payload.user.toLowerCase()
        );

        if (!user) {
          lobby.players.push({
            host: false,
            username: action.payload.user,
            slot: action.payload.slot,
            team: action.payload.team
          });
        } else {
          user.host = false;
          user.username = action.payload.user;
          user.slot = action.payload.slot;
          user.team = action.payload.team;
        }

        // Sort users by slot
        lobby.players = lobby.players.sort((a, b) => a.slot - b.slot);

        // init scores
        switch (lobby.teamMode) {
          case 'HeadToHead':
            ctx.dispatch(
              new SetTeamScore({
                channel: action.payload.channelName,
                team: action.payload.user,
                score: 0,
              })
            );
            break;
          case 'TeamVs':
            const teams = this.getTeams(lobby.scores);
            if (teams.indexOf(action.payload.team) === -1) {
              ctx.dispatch(
                new SetTeamScore({
                  channel: action.payload.channelName,
                  team: action.payload.team,
                  score: 0,
                })
              );
            }
        }
      })
    );
  }

  @Action(MoveUser)
  async moveUser(ctx: StateContext<MultiplayerStateModel>, action: MoveUser) {
    ctx.setState(
      produce(ctx.getState(), draft => {
        const player = draft.multiplayerLobbies[
          action.payload.channelName
        ].players.find(e => e.username === action.payload.user);
        player.slot = action.payload.slot;

        // Sort users by slot
        draft.multiplayerLobbies[
          action.payload.channelName
        ].players = draft.multiplayerLobbies[
          action.payload.channelName
        ].players.sort((a, b) => a.slot - b.slot);
      })
    );
  }
  
  @Action(SetWrapCondition)
  async setWrapCondition(
    ctx: StateContext<MultiplayerStateModel>,
    action: SetWrapCondition
  ) {
    ctx.setState(
      produce(ctx.getState(), draft => {
        const channelName = this.store.selectSnapshot(ChannelState.currentChannel);
        draft.multiplayerLobbies[channelName].wrapCondition = action.payload;
      })
    );
  }

  @Action(SetAutoshow)
  async setAutoshow(
    ctx: StateContext<MultiplayerStateModel>,
    action: SetAutoshow
  ) {
    ctx.setState(
      produce(ctx.getState(), draft => {
        const channelName = this.store.selectSnapshot(ChannelState.currentChannel);
        draft.multiplayerLobbies[channelName].autoshow = action.payload;
      })
    );
  }

  @Action(SetAutowrap)
  async setAutowrap(
    ctx: StateContext<MultiplayerStateModel>,
    action: SetAutowrap
  ) {
    ctx.setState(
      produce(ctx.getState(), draft => {
        const channelName = this.store.selectSnapshot(ChannelState.currentChannel);
        draft.multiplayerLobbies[channelName].autowrap = action.payload;
      })
    );
  }

  @Action(SetTeam)
  async setTeam(ctx: StateContext<MultiplayerStateModel>, action: SetTeam) {
    ctx.setState(
      produce(ctx.getState(), draft => {
        const lobby = draft.multiplayerLobbies[action.payload.channel];
        const team = action.payload.team;

        const player = lobby.players.find(e => e.username === action.payload.username);
        player.team = team;

        // init scores
        if (lobby.teamMode === 'TeamVs') {
          const teams = this.getTeams(lobby.scores);
          if (teams.indexOf(team) === -1) {
            ctx.dispatch(
              new SetTeamScore({
                channel: action.payload.channel,
                team: team,
                score: 0,
              })
            )
          }
        }
      })
    );
  }

  @Action(SetRoomName)
  async setRoomName(
    ctx: StateContext<MultiplayerStateModel>,
    action: SetRoomName
  ) {
    ctx.setState(
      produce(ctx.getState(), draft => {
        draft.multiplayerLobbies[action.payload.channel].roomName =
          action.payload.name;
      })
    );
  }

  @Action(SetFreeMod)
  async setFreeMod(
    ctx: StateContext<MultiplayerStateModel>,
    action: SetFreeMod
  ) {
    ctx.setState(
      produce(ctx.getState(), draft => {
        draft.multiplayerLobbies[action.payload.channel].freemod =
          action.payload.freemod;
      })
    );
  }

  @Action(SetMods)
  async setMods(ctx: StateContext<MultiplayerStateModel>, action: SetMods) {
    ctx.setState(
      produce(ctx.getState(), draft => {
        draft.multiplayerLobbies[action.payload.channel].mods =
          action.payload.mods;
      })
    );
  }

  @Action(SetTeamMode)
  async setTeamMode(
    ctx: StateContext<MultiplayerStateModel>,
    action: SetTeamMode
  ) {
    ctx.setState(
      produce(ctx.getState(), draft => {
        draft.multiplayerLobbies[action.payload.channel].teamMode =
          action.payload.mode;
      })
    );
    
    // init scores
    ctx.dispatch(
      new ReInitTeamScores({
        channel: action.payload.channel,
      })
    );
  }

  @Action(SetBeatmap)
  async setBeatmap(
    ctx: StateContext<MultiplayerStateModel>,
    action: SetBeatmap
  ) {
    ctx.setState(
      produce(ctx.getState(), draft => {
        draft.multiplayerLobbies[action.payload.channel].beatmapId =
          action.payload.id;
      })
    );
  }

  @Action(SetWinCondition)
  async setWinCondition(
    ctx: StateContext<MultiplayerStateModel>,
    action: SetWinCondition
  ) {
    ctx.setState(
      produce(ctx.getState(), draft => {
        draft.multiplayerLobbies[action.payload.channel].winCondition =
          action.payload.condition;
      })
    );
  }

  @Action(SetTeamScore)
  async setTeamScore(ctx: StateContext<MultiplayerStateModel>, action: SetTeamScore) {
    ctx.setState(
      produce(ctx.getState(), draft => {
        const scores = draft.multiplayerLobbies[
          action.payload.channel
        ].scores;

        const team = action.payload.team;
        const score = action.payload.score;

        const teamScore = scores.find(this.getFindTeamScore(team));
        if (!teamScore) {
          const newTeamScore = {
            team: team,
            score: score,
            alias: '',
          };
          if (team === 'red') {
            scores.unshift(newTeamScore);
          } else {
            scores.push(newTeamScore);
          }
        } else {
          teamScore.score = score;
        }
      })
    );
  }

  @Action(SetAliasTeamScore)
  async setAliasTeamScore(ctx: StateContext<MultiplayerStateModel>, action: SetAliasTeamScore) {
    ctx.setState(
      produce(ctx.getState(), draft => {
        const scores = draft.multiplayerLobbies[
          action.payload.channel
        ].scores;

        const team = action.payload.team;
        const alias = action.payload.alias;

        const teamScore = scores.find(this.getFindTeamScore(team));
        if (teamScore) {
          teamScore.alias = alias;
        } else {
          ctx.dispatch(
            new AddToast({
              severity: 'error',
              detail: 'Undefind team!'
            })
          );
        }
      })
    );
  }
  
  @Action(RemoveTeamScore)
  async removeTeamScore(ctx: StateContext<MultiplayerStateModel>, action: RemoveTeamScore) {
    ctx.setState(
      produce(ctx.getState(), draft => {
        const scores = draft.multiplayerLobbies[
          action.payload.channel
        ].scores;

        const index = scores.findIndex(this.getFindTeamScore(action.payload.team));

        if (index !== -1) {
          scores.splice(index, 1)
        } else {
          ctx.dispatch(
            new AddToast({
              severity: 'error',
              detail: 'Undefind team!'
            })
          );
        }
      })
    );
  }

  @Action(RemoveUser)
  async removeUser(
    ctx: StateContext<MultiplayerStateModel>,
    action: RemoveUser
  ) {
    const username = action.payload.user;

    ctx.setState(
      produce(ctx.getState(), draft => {
        const lobby = draft.multiplayerLobbies[action.payload.channelName];
        
        const index = lobby.players.findIndex(e => e.username === username);
        const team = lobby.players[index].team;

        lobby.players.splice(index, 1);

        // // Remove user score
        // switch (lobby.teamMode) {
        //   case 'HeadToHead':
        //     ctx.dispatch(
        //       new RemoveTeamScore({
        //         channel: action.payload.channelName,
        //         team: username,
        //       })
        //     );
        //     break;
        //   case 'TeamVs':
        //     // Check is players in this team
        //     const teams = this.getTeams(lobby.players);
        //     if (teams.indexOf(team) === -1) {
        //       ctx.dispatch(
        //         new RemoveTeamScore({
        //           channel: action.payload.channelName,
        //           team: team,
        //         })
        //       );
        //     }
        // }
      })
    );
  }

  @Action(ChangeChannelName)
  changeChannelName(
    ctx: StateContext<MultiplayerStateModel>,
    action: ChangeChannelName
  ) {
    ctx.setState(
      produce(ctx.getState(), draft => {
        draft.multiplayerLobbies[action.payload.newName] = draft.multiplayerLobbies[action.payload.channelName];
        delete draft.multiplayerLobbies[action.payload.channelName];

        draft.multiplayerLobbies[action.payload.newName].mpId = action.payload.newName;
      })
    );
  }

  @Action(Logout)
  async logout(ctx: StateContext<MultiplayerStateModel>) {
    ctx.setState(
      produce(ctx.getState(), draft => {
        draft.multiplayerLobbies = {};
      })
    );
  }

  @Action(ReInitTeamScores)
  async reInitTeamScores(
    ctx: StateContext<MultiplayerStateModel>,
    action: ReInitTeamScores
  ) {
    ctx.setState( 
      produce(ctx.getState(), draft => {
        const lobby = draft.multiplayerLobbies[action.payload.channel];

        switch (lobby.teamMode) {
          case 'HeadToHead':
            lobby.scores = [];
            lobby.players.forEach(player => {
              ctx.dispatch(
                new SetTeamScore({
                  channel: action.payload.channel,
                  team: player.username,
                  score: 0,
                })
              );
            });
            break;
          case 'TeamVs':
            lobby.scores = [];

            const teams = this.getTeams(lobby.players).filter(team => team);
            teams.forEach(team => {
              ctx.dispatch(
                new SetTeamScore({
                  channel: action.payload.channel,
                  team: team,
                  score: 0,
                })
              );
            });
            break;
          default:
            lobby.scores = [];
        }
      })
    );
  }  

  getFindTeamScore(team: string) {
    const replaceSpaces = s => s.replace(' ', '_');
    return (s => replaceSpaces(s.team) === replaceSpaces(team) || (s.alias && replaceSpaces(s.alias) === replaceSpaces(team)));
  }

  getTeams(objs: TeamObj[]) {
    return Array.from(new Set(objs.map(player => player.team)));
  }
}
