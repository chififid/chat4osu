import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Store } from '@ngxs/store';
import { MpLobby, MpUser, WrapCondition} from '../../store/states/multiplayer.state';

@Component({
  selector: 'app-mp-user-bar',
  templateUrl: './mp-user-bar.component.html',
  styleUrls: ['./mp-user-bar.component.scss']
})
export class MpUserBarComponent implements OnInit {
  @Input()
  mpLobby: MpLobby;

  @Input()
  mpStartTime: number;

  @Output()
  setWrapCondition: EventEmitter<WrapCondition> = new EventEmitter();
  
  @Output()
  start: EventEmitter<number> = new EventEmitter();

  @Output()
  refresh = new EventEmitter();

  @Output()
  abort = new EventEmitter();

  @Output()
  show = new EventEmitter();

  @Output()
  wrap = new EventEmitter();

  @Output()
  setAutowrap = new EventEmitter();

  @Output()
  setAutoshow = new EventEmitter();
  
  wrapConditions = Object.values(WrapCondition);

  viewPortItems: MpUser[];

  constructor(public store: Store) {}

  ngOnInit(): void { }
}
