import { Injectable } from '@angular/core';
import { MpUser, TeamScore, WrapCondition } from '../store/states/multiplayer.state';
import * as CryptoJS from 'crypto-js';
import { TextAst } from '@angular/compiler';

interface LinkedOsuScores { 
  [team: string]: any;
}

interface linkedScoreResults { 
  [team: string]: number;
}

const MATCH_API = 'https://osu.ppy.sh/api/get_match';

@Injectable({ providedIn: 'root' })
export class AliasService {
  wrapScore(
    mpId: string,
    apiKey: string,
    players: MpUser[],
    lastScores: TeamScore[],
    wrapCondition: WrapCondition,
  ): [TeamScore, linkedScoreResults] {
    const lobbyId = this.getLobbyId(mpId);

    const matchUrl = new URL(MATCH_API);
    matchUrl.searchParams.set('k', apiKey);
    matchUrl.searchParams.set('mp', lobbyId.toString());

    const request = new XMLHttpRequest();
    request.open('GET', matchUrl.toString(), false);
    request.send(null);

    if (request.status === 200) {
      const data = JSON.parse(request.responseText);
      if (data.games.length === 0) {
        throw new Error('Undefined game for wrap!');
      }
      
      const lastMatch = data.games.pop();
      const osuScores = lastMatch.scores;

      let linkedOsuScores;
      try {
        linkedOsuScores = this.linkTeamsToOsuScores(osuScores, players);
      } catch (error) {
        throw  new Error('Can\'t match up users and scores!');
      }

      const [wonTeamName, linkedResults] = this.getWonTeam(linkedOsuScores, wrapCondition);
      if (!wonTeamName) {
        return [, linkedResults];
      }
      const lastScore = { ...lastScores.find(score => score.team === wonTeamName) };
      lastScore.score += 1;
      return [lastScore, linkedResults];
    } else {
      throw new Error('Alias\' autocomplete server undefined!');
    }

    return;
  };

  pick(args: string[], playersCount: number, refereeKey: string, myUsername: string) {
    this.checkCountArgs(args, 1);

    // if (playersCount < 2) {
    //   throw new Error('It\'s not a match time!');
    // }

    const pick = args[0];
  
    const request = new XMLHttpRequest();
    const aliasApiUrl = this.getAliasApiUrl(refereeKey, myUsername)

    if (!aliasApiUrl) {
      throw new Error('Invalid referee key!');
    }
  
    request.open('GET', aliasApiUrl, false);
    request.send(null);
      
    if (request.status === 200) {
      const data = JSON.parse(request.responseText);
      const pickData = data[pick];

      try {
        const forcedCommand = this.getForcedCommand(pickData.forced);
        const modsCommand = this.getModsCommand(pickData.mods);
        const diffId = this.getRandomItem(pickData.maps)
        const pickCommand = this.getPickCommand(diffId);
        return [pickCommand, modsCommand, forcedCommand];
      } catch (error) {
        throw new Error('Unvalid json!');
      }
    } else {
      throw new Error('Aliases autocomplete server undefined!');
    }
  }

  getAliasArgs(args: string[]) {
    this.checkCountArgs(args, 2);

    return args;
  }

  getDelArgs(args: string[]) {
    this.checkCountArgs(args, 1);

    return args[0];
  }

  getInitArgs(args: string[]): [string, number]  {
    this.checkCountArgs(args, 2);

    const score = parseInt(args[1]);
    if (isNaN(score)) {
      throw new Error('Fucked args! Second arg must be integer!')
    }

    return [args[0], +args[1]];
  }

  scoresToString(scores: TeamScore[], args: string[]) {
    this.checkCountArgs(args, 0);

    const teamNames = scores.map(score => score.alias ? score.alias : score.team);
    if (scores.length === 2) {
      return `${teamNames[0]} | ${scores[0].score} : ${scores[1].score} | ${teamNames[1]}`;
    }

    return scores.map(score => `${score.alias ? score.alias : score.team} : ${score.score}`).join(' | ');
  }

  resultsToString(linkedResults: linkedScoreResults, wrapCondition: WrapCondition, scores: TeamScore[]) {
    let sortedEntries;
    let condition;

    const entries = Object.entries(linkedResults);
    switch (wrapCondition) {
      case WrapCondition.Accuracy: case WrapCondition.MissAccuracy: case WrapCondition.Score: case WrapCondition.Combo:
        sortedEntries = entries.sort(([, a], [, b]) => b - a);
        break;
      case WrapCondition.Miss:
        sortedEntries = entries.sort(([, a], [, b]) => a - b);
    }

    let results = [];
    const sortedKeys = sortedEntries.map(entrie => entrie[0]);

    sortedKeys.forEach(team => {
      let result;

      switch (wrapCondition) {
        case WrapCondition.Accuracy:
          // The simpliest way, nevermind
          result = (linkedResults[team] * 100).toFixed(3).slice(0, -1) + '%';
          condition = 'accuracy';
          break;
        case WrapCondition.MissAccuracy:
          result = Math.round(linkedResults[team] * 1000).toString();
          condition = 'points';
          break;
        case WrapCondition.Miss:
          result = linkedResults[team].toString();
          condition = 'miss';
          break;
        case WrapCondition.Combo:
          result = Math.round(linkedResults[team]).toString();
          condition = 'combo';
          break;
        case WrapCondition.Score:
          result = Math.round(linkedResults[team]).toString();
          condition = 'score';
          break;
      }
      
      let teamScore = scores.filter(score => score.team === team)[0];
      let teamName = teamScore.alias ? teamScore.alias : teamScore.team;
      results.push(`${teamName}: ${result}`);
    })
    
    return results.join('; ') + ` (${condition})`;
  }

  getWonTeam(
    linkedOsuScores: LinkedOsuScores,
    wrapCondition: WrapCondition,
  ): [string, linkedScoreResults] {
    let linkedResults: linkedScoreResults = {};
    Object.keys(linkedOsuScores).forEach((team) => {
      const scores = linkedOsuScores[team];
      
      let sum = 0;
      scores.forEach(score => {
        const count300 = Number(score.count300);
        const count100 = Number(score.count100);
        const count50 = Number(score.count50);
        const countMiss = Number(score.countmiss);
        switch (wrapCondition) {
          case WrapCondition.Accuracy:
            sum += this.calcAccuracy(count300, count100, count50, countMiss);
            break;
          case WrapCondition.Miss:
            sum += countMiss;
            break;
          case WrapCondition.MissAccuracy:
            sum += this.calcAccuracy(count300, count100, count50, countMiss * 5);
            break;
          case WrapCondition.Score:
            sum += Number(score.score);
            break;
          case WrapCondition.Combo:
            sum += Number(score.maxcombo);
            break;
        }
      });
      
      if (wrapCondition in [WrapCondition.Accuracy, WrapCondition.MissAccuracy, WrapCondition.Miss]) {
        linkedResults[team] = sum / scores.length;
      } else {
        linkedResults[team] = sum;
      }
    });

    let winnersData;
    switch (wrapCondition) {
      case WrapCondition.Accuracy: case WrapCondition.MissAccuracy: case WrapCondition.Score: case WrapCondition.Combo:
        winnersData = Math.max.apply(Math, Object.values(linkedResults));
        break;
      case WrapCondition.Miss:
        winnersData = Math.min.apply(Math, Object.values(linkedResults));
    }

    const winedTeams = Object.entries(linkedResults).filter(([, value]) => value === winnersData);
    if (winedTeams.length !== 1) {
      return [, linkedResults];
    }
    return [winedTeams[0][0], linkedResults];
  }
  
  calcAccuracy(count300: number, count100: number, count50: number, countMiss: number) {
    return (300 * count300 + 100 * count100 + 50 * count50) / 
    (300 * (count300 + count100 + count50 + countMiss));
  }

  linkTeamsToOsuScores(osuScores: any[], players: MpUser[]): LinkedOsuScores {
    const result = {};
    const linkedPlayers = this.linkPlayersToSlots(players);
    osuScores.map(osuScore => {
      const player = linkedPlayers[Number(osuScore.slot) + 1];
      const team = player.team ? player.team : player.username;
      if (!(team in result)) {
        result[team] = [];
      }
      result[team].push(osuScore);
    })
    console.log(result);
    return result;
  }

  linkPlayersToSlots(players: MpUser[]) {
    const result = {};
    players.map(player => {
      result[player.slot] = player;
    });
    return result;
  }

  getRandomItem(l: number[]) {
    return l[Math.floor((Math.random()*l.length))];
  }

  getPickCommand(diffId: number) {
    return '!mp map ' + diffId.toString();
  }

  getModsCommand(mods: string) {
    return '!mp mods ' + mods;
  }

  getForcedCommand(forced: string) {
    return forced + ' is forced, else is optional.';
  }

  getLobbyId(mpId: string) {
    return Number(mpId.split('_')[1]);
  }

  getAliasApiUrl(refereeKey: string, myUsername: string) {
    const bytes = CryptoJS.AES.decrypt(refereeKey, this.reverse(myUsername));
    return bytes.toString(CryptoJS.enc.Utf8);
  }

  checkCountArgs(args: string[], count: number) {
    if (args.length !== count) {
      throw new Error(`Fucked args! Must be ${count}!`);
    }
  }

  reverse(s: string){
    return s.split('').reverse().join('');
  }
}